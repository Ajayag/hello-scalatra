from tomcat:7.0

maintainer [ajayagdokerid]

COPY target/*.war /usr/local/tomcat7/webapps/hello-scalatra.war

EXPOSE 8080  

CMD ["catalina.sh", "run"] 
